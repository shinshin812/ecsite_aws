<?php
require('../common/function.php');

$db = dbConnect();

if(isset($_POST['login']) && !empty($_POST['admin_name'])){
$stmt = $db->prepare("SELECT * FROM admin WHERE admin_name= ? ");
$stmt->execute(array($_POST['admin_name']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$hash = password_hash("{$_POST['admin_password']}", PASSWORD_DEFAULT);

if(isset($_POST['admin_name']) && isset($_POST['admin_email'])){
  if(isset($_POST['admin_password'])){
    if($_POST['admin_name'] != $row['admin_name'] || $_POST['admin_email'] != $row['admin_email'] || $_POST['admin_password'] != $row['admin_password']){
        $loginCheck = "※入力が間違っています";
    }
  }
}
if(isset($_POST['admin_name']) && isset($_POST['admin_email']) && isset($_POST['admin_password'])){
  if($_POST['admin_email'] == $row['admin_email']){
    if(password_verify($_POST['admin_password'],$row['admin_password'])){
      if($_POST['admin_name'] = $row['admin_name']){
        $_SESSION['admin_name'] = $_POST['admin_name'];
        header('location: ../admin/admin_index.php');
        exit();
      }
    }
  }
}

}
?>
<html>
<title>管理者ログイン</title>
<style>
body{
  background: #e9e9e9;
  color: #5e5e5e;
}
@media only screen and (min-width: 460px){
.bg-image{
  background-image: url("../img/HNCK4963.jpg");
  background-size: cover;
}
.bg-mask {
  background: rgba(255,255,255,0.5);
}
}
.form-wrapper {
  background: #fafafa;
  padding: 90 20px;
}
.form-control{
  margin-bottom: 10px;
}
</style>
<body>
  <div class="bg-image">
    <div class="bg-mask">
      <div class="container-fluid">
        <div class="row">
         <div class="col-12 col-lg-6 offset-lg-3 mt-5 mb-5">
          <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkAdminName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkAdminEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkAdminPassword() ?></div>
      <?php if(isset($loginCheck)): ?>
      <div class="alert-danger" role="alert"><?php echo $loginCheck ?></div>
    <?php endif ?>
    <h1 class="text-primary">管理者ログイン</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="admin_name" value="<?php echo displayAdminName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="email" name="admin_email" value="<?php echo displayAdminEmail() ?>">
    </div>
    <div class="form-group">
      パスワード
    <input class="form-control" placeholder="Password" name="admin_password" type="password" class="field" id="password" value="<?php echo displayAdminPassword() ?>">
    <input name="check_password" type="checkbox" id="password-check">
    パスワードを表示する
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name=login>ログイン</button>
  </form>
</br>
  <a href="login.php">ユーザーログインはこちら</a>
        </div>
      </div>
    </div>
   </div>
  </div>
 </div>
</div>
</body>
<script>
 const pwd = document.getElementById('password');
 const pwdCheck = document.getElementById('password-check');
 pwdCheck.addEventListener('change', function() {
     if(pwdCheck.checked) {
         pwd.setAttribute('type', 'text');
     } else {
         pwd.setAttribute('type', 'password');
     }
 }, false);
 </script>
</html>

