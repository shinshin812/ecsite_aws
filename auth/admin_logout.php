<?php
require('../common/function.php');

$_SESSION['admin_name'] = array();
session_destroy();
 ?>
 <html>
 <title>管理者ログアウト</title>
 <style>
     body{
       background: #e9e9e9;
       color: #5e5e5e;
     }
 </style>
 <body>
<div class="container-fluid">
 <div class="row">
  <div class="col-lg-12 mt-5">
 <div class="text-center">
   <h2>ログアウトしました</h2>
   <input class="btn btn-info btn-lg" type="submit" value="戻る" onclick="location.href='admin_login.php'">
   </div>
  </div>
 </div>
</div>
 </body>
 </html>
