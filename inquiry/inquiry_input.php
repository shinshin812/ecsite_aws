<?php
require('../common/function.php');
$db = dbConnect();

if(isset($_POST['confirm'])){
$stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
$stmt->execute(array($_SESSION['name']));
$row1 = $stmt->fetch(PDO::FETCH_ASSOC);

    if(isset($_POST['email']) && isset($_POST['message']) && isset($_POST['title'])){
        if($_POST['email'] != $row1['email']){
            $loginCheck = "※入力が間違っています";
      }
    }

    if(isset($_POST['email']) && isset($_POST['message']) && isset($_POST['title'])){
      if($_POST['message'] != "" && $_POST['title'] != ""){
      if($_POST['email'] == $row1['email']){
            $_SESSION['inquiry'] = $_POST;
            header('location: inquiry_confirm.php');
            exit();
        }
      }
    }
}
 ?>
<html>
<title>お問い合わせ</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      padding: 20 20px;
    }
    .form-control{
      margin-bottom: 10px;
    }
    .font{
      margin-top: 6px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="../index.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="../products/product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <form method="post" action="../cart/cart.php">
              <div class="font">
              <input class="btn btn-primary btn-sm fas" type="submit" name="nakami" value="&#xf07a; cart">
              </div>
            </form>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form" action="../products/favorite_list.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
              </form>
            </li>
          <?php endforeach ?>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form1" action="../orders/order_history.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
              </form>
            </li>
            <?php endforeach ?>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="../auth/logout.php">Logout</a>
          <?php elseif(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="../auth/login.php">Login</a>
          <?php endif ?>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid mt-5">
    <div class="row">
     <div class=" col-12 col-lg-8 offset-lg-2 mt-5">     
      <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkMessage() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkTitle() ?></div>
      <?php if(isset($loginCheck)): ?>
      <div class="alert-danger" role="alert"><?php echo $loginCheck ?></div>
    <?php endif ?>
    <h1>お問い合わせ</h1>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="email" name="email" value="<?php echo displayEmail() ?>">
    </div>
    <div class="form-group">
      題名
    <input class="form-control" placeholder="Title" type="text" name="title" value="<?php echo displayTitle() ?>">
    </div>
    <div class="form-group">
      本文
    <textarea class="form-control" rows="5" placeholder="Message" type="text" name="message" value="<?php echo displayMessage() ?>"></textarea>
    </div>
    <input class="btn btn-info btn-block" type="submit" name="confirm" value="確認"></br>
  </form>
  </br>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

