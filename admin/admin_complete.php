<?php
require('../common/function.php');

$db = dbConnect();

$adminName = $_SESSION['admin_name'];
$adminEmail = $_SESSION['admin_email'];
$hash = password_hash("{$_SESSION['admin_password']}", PASSWORD_DEFAULT);

$sql="INSERT INTO admin (admin_name, admin_email, admin_password) VALUES (:adminName, :adminEmail, :hash)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':adminName',$adminName, PDO::PARAM_STR);
$stmt->bindParam(':hash',$hash, PDO::PARAM_STR);
$stmt->bindParam(':adminEmail',$adminEmail, PDO::PARAM_STR);
$stmt->execute();
?>
<html>
<title>管理者登録完了</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      padding: 20 20px;
    }
</style>
<body>
  <div class="container-fluid">
    <div class="row">
     <div class="col-12 col-lg-8 offset-lg-2 mt-5">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <h2>登録完了</h2>
  <div class="form-group">
    名前</br>
  <?php echo $_SESSION['admin_name'] ?>
  </div>
  <div class="form-group">
    メールアドレス</br>
  <?php echo $_SESSION['admin_email'] ?>
  </div>
  <div class="form-group">
    パスワード</br>
  <?php echo $_SESSION['admin_password'] ?>
  </div>
<a href="../auth/admin_login.php">管理者ログインへ</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

