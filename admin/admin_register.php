<?php
require('../common/function.php');
adminCheckOk();
?>
<html>
<title>管理者登録</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      padding: 20 20px;
    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container-fluid">
    <div class="row">
     <div class="col-12 col-lg-8 offset-lg-2 mt-5">
      <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkAdminName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkAdminEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkAdminPassword() ?></div>
    <h1 class="text-dark">ユーザー登録</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="admin_name" value="<?php echo displayAdminName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="email" name="admin_email" value="<?php echo displayAdminEmail() ?>">
    </div>
    <div class="form-group">
      パスワード
    <input class="form-control" placeholder="Password" name="admin_password" type="password" class="field" id="password" value="<?php echo displayAdminPassword() ?>">
    <input name="check_password" type="checkbox" id="password-check">
    パスワードを表示する
    </div>
    <input class="btn btn-dark btn-lg" type="submit" value="確認">
  </form>
  </br>
  <a href="../auth/admin_login.php">管理者ログインはこちら</a></br>
  <a href="../products/product_register.php">商品登録はこちら</a></br>
  <a href="../item/item_register.php">アイテム登録はこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
 const pwd = document.getElementById('password');
 const pwdCheck = document.getElementById('password-check');
 pwdCheck.addEventListener('change', function() {
     if(pwdCheck.checked) {
         pwd.setAttribute('type', 'text');
     } else {
         pwd.setAttribute('type', 'password');
     }
 }, false);
 </script>
</html>

