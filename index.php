<?php
require('common/function.php');
$db = dbConnect();

$sql = "SELECT * FROM product ORDER BY id DESC limit 2";
$stmt = $db->prepare($sql);
$stmt->execute();

    $sql1 = "SELECT product.id,product.product_name,product.product_image,product.image_detail1,product.image_detail2,product.image_detail3,product.image_detail4,product.price,product.product_introduction,sum(order_detail.num) FROM product INNER JOIN order_detail ON product.id = order_detail.product_id GROUP BY product_id ORDER BY sum(num) DESC LIMIT 5";
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();
 ?>
<html>
<title>ホーム</title>
<style>
    body{
      background: linear-gradient(-135deg, #E4A972, #9941D8) fixed;
      color: #FFFFFF;
    }
    .font{
      font-family: "Comic Sans MS";
    }
    .bg-image{
      background-image: url("img/apple-4217863_1280.jpg");
      background-size: cover;
    }
    .popular{
      height: 350px;
      width: 300px;
      display: block;
      margin-left: auto;
      margin-right: auto; 
    }
    #animation {
      font-size: 125px;
      text-align:center;
    }
@media screen and (max-width: 768px){
    #animation {
      font-size: 70px;
      text-align:center;
    }
}

.fadeInDown {
 -webkit-animation-fill-mode:both;
 -ms-animation-fill-mode:both;
 animation-fill-mode:both;
 -webkit-animation-duration:1s;
 -ms-animation-duration:1s;
 animation-duration:1s;
 -webkit-animation-name: fadeInDown;
 animation-name: fadeInDown;
 visibility: visible !important;
}
@-webkit-keyframes fadeInDown {
 0% { opacity: 0; -webkit-transform: translateY(-20px); }
 100% { opacity: 1; -webkit-transform: translateY(0); }
}
@keyframes fadeInDown {
 0% { opacity: 0; -webkit-transform: translateY(-20px); -ms-transform: translateY(-20px); transform: translateY(-20px); }
 100% { opacity: 1; -webkit-transform: translateY(0); -ms-transform: translateY(0); transform: translateY(0); }
}
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="#">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <?php if(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="users/user_register.php">sign up</a>
          <?php endif ?>
          </li>
          <li class="nav-item">
            <?php if(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="auth/login.php">login</a>
          <?php endif ?>
          </li>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="auth/logout.php">Logout</a>
          <?php endif ?>
          </li>
          <div class="dropdown">
  <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    MENU
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="products/product_list.php">LIST</a>
    <?php if(!empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="auth/logout.php">Logout</a>
  <?php elseif(empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="auth/login.php">Login</a>
  <?php endif ?>
  <?php if(empty($_SESSION['name'])): ?>
  <a class="dropdown-item" href="users/user_register.php">sign up</a>
<?php endif ?>
<a class="dropdown-item" href="products/ranking.php">ranking</a>
  <?php if(!empty($_SESSION['name'])): ?>
  <a class="dropdown-item" href="inquiry/inquiry_input.php">お問い合わせ</a>
<?php endif ?>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="index.php">home</a>
  </div>
    </div>
        </ul>
      </div>
    </div>
  </nav>

  <div class="font">
    <div class="bg-image">
    </br></br>
          <h1>Shopping Site</h1>
          </br>
<div class="container-fluid">
            <div class="row">
<div class="col-lg-8 offset-lg-2">
            <p class="message" id="animation">Welcome to my ECsite!</p>
          <form method="post" action="products/product_list.php">
            <div class="input-group mb-3">
              <input class="form-control" placeholder="Search" type="text" name="search" id="hogehoge">
              <input class="btn btn-primary fas" type="submit" name="search-button" value="&#xf002;">
            </form>
              </div>
            </br>
              <h1 class="subtitle">all item</h2>
          <form method="post" action="products/product_list.php">
            <div class="form-group">
              <input class="btn btn-outline-primary btn-lg" type="submit" name="submit" value="全ての商品一覧を見る ➡︎">
            </div>
          </form>
        </br>
        </div>
</div>
</div>
</div>
          <div class="container-fluid mt-5">
            <div class="row">
        <?php foreach($stmt as $row): ?>
          <div class="col-lg-6">
            <h2 class="subtitle">New product</h2>
            <div class="card img-thumbnail">
              <img class="card-img-top" src="<?php echo $row['product_image'] ?>">
            </div>
            <form method="post" action="products/product_detail.php">
              <div class="form-group">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="新着商品を見る">
              </div>
              <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
              <input type="hidden" name="product_name" value="<?php echo $row['product_name'] ?>">
              <input type="hidden" name="product_image" value="<?php echo "../".$row['product_image'] ?>">
              <input type="hidden" name="image_detail1" value="<?php echo "../".$row['image_detail1'] ?>">
              <input type="hidden" name="image_detail2" value="<?php echo "../".$row['image_detail2'] ?>">
              <input type="hidden" name="image_detail3" value="<?php echo "../".$row['image_detail3'] ?>">
              <input type="hidden" name="image_detail4" value="<?php echo "../".$row['image_detail4'] ?>">
              <input type="hidden" name="product_introduction" value="<?php echo $row['product_introduction'] ?>">
              <?php $tax = 1.1;
                    $price = $row['price'] * $tax;
                    $product_price = number_format($price); ?>
              <input type="hidden" name="price" value="<?php echo $price ?>">
            </form>
          </div>
        <?php endforeach ?>

<h2 class="subtitle">popular</h2>
<div class="slider">
        <?php foreach($stmt1 as $row1): ?>
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="popular">
            <img src="<?php echo $row1['product_image'] ?>">
        </div>
          <form method="post" action="products/product_detail.php">
            <input type="hidden" name="id" value="<?php echo $row1['id'] ?>">
            <input type="hidden" name="product_name" value="<?php echo $row1['product_name'] ?>">
            <input type="hidden" name="product_image" value="<?php echo "../".$row1['product_image'] ?>">
            <input type="hidden" name="image_detail1" value="<?php echo "../".$row1['image_detail1'] ?>">
            <input type="hidden" name="image_detail2" value="<?php echo "../".$row1['image_detail2'] ?>">
            <input type="hidden" name="image_detail3" value="<?php echo "../".$row1['image_detail3'] ?>">
            <input type="hidden" name="image_detail4" value="<?php echo "../".$row1['image_detail4'] ?>">
            <input type="hidden" name="product_introduction" value="<?php echo $row1['product_introduction'] ?>">
            <?php $tax = 1.1;
                      $price = $row1['price'] * $tax;
                      $product_price = number_format($price); ?>
            <input type="hidden" name="price" value="<?php echo $price ?>">
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="こちらの人気商品を見る">
            </div>
          </form>
        </div>
        </div>
      </div>
      <?php endforeach ?>
      </div>

        <div class="col-lg-4">
          <h2 class="subtitle">sweets</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "img/illustrain01-christmas03.png" ?>">
        </div>
          <form method="post" action="products/product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'sweets'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
        <div class="col-lg-4">
          <h2 class="subtitle">fruit</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "img/illustrain04-food13.png" ?>">
        </div>
          <form method="post" action="products/product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'fruit'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
        <div class="col-lg-4">
          <h2 class="subtitle">vegetable</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "img/6607.png" ?>">
        </div>
          <form method="post" action="products/product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'vegetable'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <footer class="py-5 bg-dark">
    <div class="container">
      <div class="text-center">
        <?php if(!empty($_SESSION['name'])): ?>
        <a class="text-white" href="inquiry/inquiry_input.php">お問い合わせ</a>
      <?php endif ?>
      </div>
    </div>
    <p class="m-0 text-center text-white">@God Mountain</p>
    </footer>
</body>
<script>
$('#animation').css('visibility','hidden');
$(window).scroll(function(){
 var windowHeight = $(window).height(),
     topWindow = $(window).scrollTop();
 $('#animation').each(function(){
  var targetPosition = $(this).offset().top;
  if(topWindow > targetPosition - windowHeight + 100){
   $(this).addClass("fadeInDown");
  }
 });
});

$(function() {
  // 入力補完候補の単語リスト
  var wordlist = [
    "sweets",
    "fruit",
    "vegetable"
  ];

  // 入力補完を実施する要素に単語リストを設定
  $( "#hogehoge" ).autocomplete({
    source: wordlist
  });
});

$(document).ready(function(){
    $('.slider').bxSlider({
        auto: true,
        pause: 5000,
    });
});
</script>
</html>

