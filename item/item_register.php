<?php
require('../common/function.php');
checkItemOk();

if(isset($_POST['back'])){
  unlink("../img/".$_SESSION['item_image1']['name']);
  unlink("../img/".$_SESSION['item_image2']['name']);
  unlink("../img/".$_SESSION['item_image3']['name']);
  unlink("../img/".$_SESSION['item_image4']['name']);
  unlink("../img/".$_SESSION['item_image5']['name']);
  unlink("../img/".$_SESSION['item_image6']['name']);
}
 ?>
<html>
<title>アイテム登録</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      padding: 20 20px;
    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container-fluid">
    <div class="row">
     <div class="col-12 col-lg-8 offset-lg-2 mt-5">
      <div class="text-center">
       <div class="form-wrapper">
<h1 class="text-info">アイテム登録</h1>
<form method="post" action="" enctype="multipart/form-data">
  <div class="form-group">
    アイテム画像</br>
  <input type="file" name="item_image1">
  <input type="file" name="item_image2">
  <input type="file" name="item_image3">
  <input type="file" name="item_image4">
  <input type="file" name="item_image5">
  <input type="file" name="item_image6">
  </div>
  <input class="btn btn-info btn-lg" type="submit" name="register" value="登録">
</form>
<a href="../auth/admin_login.php">管理者ログインはこちら</a></br>
  <a href="../admin/admin_register.php">管理者登録はこちら</a></br>
  <a href="../products/product_register.php">商品登録はこちら</a>
        </div>
      </div>
    </div>
 </div>
</div>
</body>
</html>
