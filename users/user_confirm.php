<?php
require('../common/function.php');
?>
<html>
<title>ユーザー確認</title>
<style>
body{
  background: #e9e9e9;
  color: #5e5e5e;
}
    .form-wrapper {
      background: #fafafa;
      padding: 20 20px;
    }
    .form-item{
      margin-bottom: 15px;
    }
</style>
<body>
  <div class="container-fluid">
    <div class="row">
     <div class=" col-12 col-lg-8 offset-lg-2 mt-5">
      <div class="text-center">
  <form method="post" action="user_complete.php">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <div class="form-group">
    名前</br>
  <?php echo $_SESSION['name'] ?>
  </div>
  <div class="form-group">
    メールアドレス</br>
  <?php echo $_SESSION['email'] ?>
  </div>
  <div class="form-group">
    パスワード</br>
  <?php echo $_SESSION['password'] ?>
  </div>
  <div class="form-group">
    住所</br>
  <?php echo $_SESSION['address'] ?>
  </div>
  <div class="form-group">
    クレジットカード番号</br>
  <?php echo $_SESSION['credit'] ?>
  </div>
  <div class="form-item">
  <input class="btn btn-info btn-lg" type="submit" value="送信">
  </div>
</form>
<form method="post" action="user_register.php">
  <input type="submit" value="戻る" name="back">
</form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

